function createCard(name, description, pictureUrl, startDate,
  endDate, location) {
  const formattedStartDate = new Date(startDate).toLocaleDateString();
  const formattedEndDate = new Date(endDate).toLocaleDateString();
  return `
    <div class="card shadow mb-4">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}<h6>
        <p class="card-text">${description}</p>
        <div class="card-footer text-muted">
        <small>Dates: ${formattedStartDate} - ${formattedEndDate}</small>
        </div>
      </div>
    </div>
  `;
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        throw new Error('Response not ok');
    } else {
        const data = await response.json();
        // const conferencesContainer = document.getElementById("conferences-container");
        // const columns = 3;
        let columnIndex = 0
          for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log("🚀 ~ file: app.js:35 ~ window.addEventListener ~ details:", details)
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts
            const endDate = details.conference.ends
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl,
              startDate, endDate, location);
            const column = document.querySelector(`#col-${columnIndex % 3}`);
            columnIndex ++;
            // const column = conferencesContainer.children[columnIndex];
            column.innerHTML += html;
            // columnIndex = (columnIndex +1) % columns;
            }
          }
        }
      } catch (e) {
      console.error('error', e);
      }
  });
