import React, { useEffect, useState } from 'react';

const log = (...items) => console.log(...items)

function ConferenceForm(props) {

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [startDate, setStartDate] = useState('');
  const handleStartDateChange = (event) => {
    const value = event.target.value;
    setStartDate(value);
  }

  const [endDate, setEndDate] = useState('');
  const handleEndDateChange = (event) => {
    const value = event.target.value;
    setEndDate(value);
  }

  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const [maxPres, setMaxPres] = useState('');
  const handleMaxPresChange = (event) => {
    const value = event.target.value;
    setMaxPres(value);
  }

  const [maxAttend, setMaxAttend] = useState('');
  const handleMaxAttendChange = (event) => {
    const value = event.target.value;
    setMaxAttend(value);
  }

  const [locations, setLocations] = useState([]);
  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {}
    data.name = name;
    data.starts = startDate;
    data.ends = endDate;
    data.description = description;
    data.max_presentations = maxPres;
    data.max_attendees = maxAttend;
    data.location = location;
    log(data);

    const locationUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      log(newConference);
      setName('');
      setStartDate('');
      setEndDate('');
      setDescription('');
      setMaxPres('');
      setMaxAttend('');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Conference</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleStartDateChange} value={startDate} placeholder="Starts" name="starts" required type="date" id="starts" className="form-control" />
              <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEndDateChange} value={endDate} placeholder="Ends" name="Ends" required type="date" id="ends" className="form-control" />
              <label htmlFor="ends">End Date</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label>">Description</label>
              <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" className="form-control" name="description" id="description" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxPresChange} value={maxPres} placeholder="Maximum Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
              <label htmlFor="max_presentations">Maximum presentations</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleMaxAttendChange} value={maxAttend} placeholder="Maximum Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
              <label htmlFor="max_attendees">Maximum attendees</label>
            </div>
            <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="location" name="locaiton" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
export default ConferenceForm;
